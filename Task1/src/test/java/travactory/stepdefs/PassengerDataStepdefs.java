package travactory.stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import travactory.pages.PassengerDataPage;

public class PassengerDataStepdefs {
    @Then("^Passenger data page is loaded$")
    public static void passengerDataPageIsLoaded() {
        PassengerDataPage passengerDataPage = new PassengerDataPage();
        passengerDataPage.shouldBeLoaded();
    }

    @When("^I fill all data with random values$")
    public void fillAllDataWithRandomValues() {
        PassengerDataPage passengerDataPage = new PassengerDataPage();
        passengerDataPage.fillAllWithRandomData();
    }

    @And("^I click on More button$")
    public void clickOnMoreButton() {
        PassengerDataPage passengerDataPage = new PassengerDataPage();
        passengerDataPage.clickMoreButton();
    }
}
