package travactory.stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import travactory.pages.AdvancedSearchPage;

public class AdvancedSearchStepDefs {

    @Then("Advanced search is opened")
    public static void advancedSearchIsOpened() {
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage();
        advancedSearchPage.shouldBeLoaded();
    }

    @Then("I hide advanced options")
    public static void hideAdvancedOptions() {
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage();
        advancedSearchPage.hideAdvancedOptions();
    }

    @And("^I click search button on advanced search$")
    public void clickSearchButtonOnAdvancedSearch() {
        AdvancedSearchPage advancedSearchPage = new AdvancedSearchPage();
        advancedSearchPage.clickSearch();
    }
}
