package travactory.pages;

import static com.codeborne.selenide.Selenide.$;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import travactory.configuration.DefaultTimeout;

public abstract class BasePage implements Page {

    private final SelenideElement loader = $(".loader-image-cover");

    public void waitForLoader() {
        loader.waitUntil(Condition.disappear, DefaultTimeout.LOADER);
    }
}
