package travactory.pages;

public interface Page {
    void shouldBeLoaded();
}
