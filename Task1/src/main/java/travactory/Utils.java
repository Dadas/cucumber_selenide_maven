package travactory;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Assert;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

public class Utils {

    public static void selectDateFromDatapicker(LocalDate date) {

        ElementsCollection calendars = $$(".ui-datepicker-calendar").shouldBe(CollectionCondition.size(13));
        SelenideElement scroll = $("#mCSB_1_dragger_vertical").$(".mCSB_dragger_bar");

        for (int i = 0; i < 13; i++) {
            ElementsCollection theSameMonthDays = calendars.get(i)
                    .findAll("td")
                    .filter(Condition.attribute("data-year", String.valueOf(date.getYear())))
                    .filter(Condition.attribute("data-month", String.valueOf(date.getMonthValue() - 1)));
            if ( theSameMonthDays.size() > 0 ) {
                ElementsCollection choose = theSameMonthDays.filter(Condition.exactText(String.valueOf(date.getDayOfMonth())));
                if ( choose.size() > 0 ) {
                    choose.first().click();
                    return;
                } else {
                    scroll.click(0, 66);
                    calendars = $$(".ui-datepicker-calendar");
                    calendars.get(i) //
                            .findAll("td") //
                            .filter(Condition.exactText(String.valueOf(date.getDayOfMonth()))) //
                            .first() //
                            .click();
                    return;
                }
            }
            if ( i > 0 && i % 2 == 0 ) {
                scroll.click(0, 66);
            }
            calendars = $$(".ui-datepicker-calendar");
        }
        Assert.fail("Date " + date + " not found in datapicker!");
    }

    public static void selectValueFromUlList(String value, SelenideElement ulElement) {
        ulElement.findAll("li").filter(Condition.exactText(value)).shouldHaveSize(1).first().click();
    }

    public static void chooseRandomValueInSelect(SelenideElement element) {
        element.click();
        ElementsCollection options = element.$$("option");
        if ( options.size() > 1 ) {
            int choose = getNumberFromRange(1, options.size() - 1);
            options.get(choose).click();
        } else {
            options.first().click();
        }
    }

    public static int getNumberFromRange(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

}
