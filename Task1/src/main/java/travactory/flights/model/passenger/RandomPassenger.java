package travactory.flights.model.passenger;

import java.time.ZoneId;
import java.util.Locale;

import com.github.javafaker.Faker;

public class RandomPassenger {

    public static PassengerData newAdult() {
        Faker faker = new Faker(new Locale("pl"));
        PassengerData passengerData = defaultPassengerData();
        passengerData.setType(PassengerType.ADULT);
        passengerData.setBirthDate(faker.date().birthday(13, 100).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        return passengerData;
    }

    public static PassengerData newChild() {
        Faker faker = new Faker(new Locale("pl"));
        PassengerData passengerData = defaultPassengerData();
        passengerData.setType(PassengerType.CHILD);
        passengerData.setBirthDate(faker.date().birthday(3, 11).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        return passengerData;
    }

    public static PassengerData newInfant() {
        Faker faker = new Faker(new Locale("pl"));
        PassengerData passengerData = defaultPassengerData();
        passengerData.setType(PassengerType.INFANT);
        passengerData.setBirthDate(faker.date().birthday(1, 1).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        return passengerData;
    }

    private static PassengerData defaultPassengerData() {
        Faker faker = new Faker(new Locale("pl"));
        PassengerData passengerData = new PassengerData();
        passengerData.setFirstName(faker.name().firstName());
        passengerData.setLastName(faker.name().lastName());
        if ( passengerData.getFirstName().endsWith("a") ) {
            passengerData.setGender(PassengerGender.FEMALE);
        } else {
            passengerData.setGender(PassengerGender.MALE);
        }
        return passengerData;
    }
}
