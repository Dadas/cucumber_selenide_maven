package travactory.flights.model.passenger;

public enum PassengerType {
    ADULT, CHILD, INFANT
}
