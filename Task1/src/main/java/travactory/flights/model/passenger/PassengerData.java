package travactory.flights.model.passenger;

import java.time.LocalDate;

import lombok.Data;

@Data
public class PassengerData {

    private String firstName;

    private String lastName;

    private LocalDate birthDate;

    private PassengerType type;

    private PassengerGender gender;
}
