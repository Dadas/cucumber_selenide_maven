package travactory.flights.model.passenger;

public enum PassengerGender {
    MALE, FEMALE
}
