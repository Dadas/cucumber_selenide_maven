package travactory.flights.model.results;

import com.codeborne.selenide.SelenideElement;

public class ResultRow {

    private SelenideElement bookingButton;

    private SelenideElement airlineName;

    private SelenideElement row;

    public ResultRow(SelenideElement row) {
        this.row = row;
        bookingButton = row.find(".button.large.red.expand.book-button.right");
        airlineName = row.find(".airline-name");
    }

    public ResultRow expand() {
        row.click();
        return this;
    }

    public ResultRow clickBookButton() {
        bookingButton.click();
        return this;
    }

    public String getAirlineName() {
        return airlineName.getText();
    }
}
