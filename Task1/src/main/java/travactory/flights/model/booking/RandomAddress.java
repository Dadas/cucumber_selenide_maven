package travactory.flights.model.booking;

import java.util.Locale;

import com.github.javafaker.Faker;

public class RandomAddress {

    public static Address get() {
        Faker faker = new Faker(new Locale("pl"));
        Address address = new Address();
        address.setCity(faker.address().city());
        address.setEmail(faker.name().username() + "@" + "test.pl");
        address.setPhoneNumber(faker.phoneNumber().phoneNumber());
        address.setStreetName(faker.address().streetName());
        address.setBuildingNumber(faker.address().buildingNumber());
        address.setPostalCode(faker.address().zipCode());
        return address;
    }
}
