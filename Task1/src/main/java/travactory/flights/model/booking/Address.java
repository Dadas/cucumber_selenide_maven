package travactory.flights.model.booking;

import lombok.Data;

@Data
public class Address {

    private String streetName;

    private String buildingNumber;

    private String postalCode;

    private String city;

    private String phoneNumber;

    private String email;

}
