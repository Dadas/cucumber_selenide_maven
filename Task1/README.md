# Patryk Dadas

My solutions for qa-recruitment.

## Task 1

### How to run tests
```
mvn clean test
```

### How to generate report from tests
```
mvn allure:serve
```
If You have any problems with reports, please use:
```
mvn allure:report
```
And open report manually in browser 

### Main used frameworks:
[Selenide] (https://github.com/codeborne/selenide)
[Cucumber] (https://github.com/cucumber/cucumber-jvm)
[Allure2] (https://github.com/allure-framework/allure2)
[Maven](https://maven.apache.org/)