# Patryk Dadas

My solutions for qa-recruitment.

## Task 2

### How to run tests
```
mvn clean test
```

### Main used frameworks:
[karate] (https://github.com/intuit/karate)
[Maven](https://maven.apache.org/)