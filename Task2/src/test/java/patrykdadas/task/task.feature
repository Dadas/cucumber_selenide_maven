Feature: Rest API test

  Background:
    * url 'https://jsonplaceholder.typicode.com'

  Scenario: User should be able to browse posts
    Given path 'posts'
    When method get
    Then status 200
    And match response contains { userId: '#number', id: '#number', title: '#string', body: '#string' }

  Scenario: User should be able to browse comments for one specific post
    Given path 'posts/1/comments'
    When method get
    Then status 200
    And match response contains {postId: 1, id: '#number', name: '#string', email: '#string', body: '#string'}

  Scenario: User should be able to create post
    Given path 'posts'
    And request {title: 'New post test', body: 'Test', userId: 1}
    When method post
    Then status 201
    And match response contains { id: '#number', title: 'New post test', body: 'Test'}

  Scenario: User should be able to delete post
    Given path 'posts',1
    And header Content-Encoding = 'gzip'
    When method delete
    Then status 200
    And match response contains { }

  Scenario: GET /posts response should have content encoding set to gzip
    Given path 'posts'
    When method get
    Then status 200
    And match header Content-Encoding == "gzip"

  Scenario: GET /posts response should return 100 records
    Given path 'posts'
    When method get
    Then status 200
    And assert response.length == 100

  Scenario: Verify that there is user with zip code equals to 23505-1337
    Given path 'users'
    When method get
    Then status 200
    And match response.[*].address.zipcode contains '23505-1337'

  Scenario: Verify number of users with website field which ends with ".org"
    Given path 'users'
    When method get
    Then status 200
    And match response.[*].website contains "#regex .*\.org"