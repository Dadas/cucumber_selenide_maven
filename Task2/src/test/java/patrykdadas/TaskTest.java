package patrykdadas;

import org.junit.runner.RunWith;

import com.intuit.karate.junit4.Karate;

import cucumber.api.CucumberOptions;

@RunWith(Karate.class)
@CucumberOptions(monochrome = true, format = { "html:target/report" })
public class TaskTest {
    // this will run all *.feature files that exist in sub-directories
}